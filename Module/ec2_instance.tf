resource "aws_instance" "ec2-instance" {
  ami           = var.instance-ami
  instance_type = "t2.micro"

  subnet_id = aws_subnet.public-subnet.id

  tags = {
    Name = "ec2-instance"
  }
}