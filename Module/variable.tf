variable "region" {
  default = "us-east-1"
}

variable "profile" {
  default = "personal"
}

variable "instance-ami" {
  default = "ami-830c94e3"
}


