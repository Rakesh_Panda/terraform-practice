resource "aws_route_table" "uat-public-route" {
  vpc_id =  "${aws_vpc.uat.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.uat_igw.id}"
  }

   tags = {
       Name = "uat-public-route"
   }
}


resource "aws_default_route_table" "uat-default-route" {
  default_route_table_id = "${aws_vpc.uat.default_route_table_id}"
  tags = {
      Name = "uat-default-route"
  }
}