resource "aws_nat_gateway" "uat-ngw" {
  allocation_id = "${aws_eip.uat_eip.id}"
  subnet_id = "${aws_subnet.public_subnet.id}"
  tags = {
      Name = "UAT Nat Gateway"
  }
}