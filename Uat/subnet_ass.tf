resource "aws_route_table_association" "uat1a" {
  subnet_id = "${aws_subnet.public_subnet.id}"
  route_table_id = "${aws_route_table.uat-public-route.id}"
}