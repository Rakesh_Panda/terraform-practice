resource "aws_internet_gateway" "uat_igw" {
 vpc_id = "${aws_vpc.uat.id}"
 tags = {
    Name = "uat-igw"
 }
}