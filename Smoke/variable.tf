variable "region" {
  default = "us-east-1"
}

variable "profile" {
  default = "personal"
}

variable "instance-ami" {
  default = "ami-830c94e3"
}

variable "vpc_cidr" {
    default = "10.0.0.0/16"
}

variable "subnet_cidr" {
    default = "10.0.0.0/24"
}