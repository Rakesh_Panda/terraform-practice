resource "aws_instance" "smoke-ec2-instance" {
  ami           = var.instance-ami
  instance_type = "t2.micro"

  subnet_id = aws_subnet.public_subnet.id

  tags = {
    Name = "smoke-ec2-instance"
  }
}