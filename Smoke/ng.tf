resource "aws_nat_gateway" "smoke-ngw" {
  allocation_id = "${aws_eip.smoke_eip.id}"
  subnet_id = "${aws_subnet.public_subnet.id}"
  tags = {
      Name = "Smoke Nat Gateway"
  }
}