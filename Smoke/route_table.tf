resource "aws_route_table" "smoke-public-route" {
  vpc_id =  "${aws_vpc.smoke.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.smoke_igw.id}"
  }

   tags = {
       Name = "smoke-public-route"
   }
}


resource "aws_default_route_table" "uat-default-route" {
  default_route_table_id = "${aws_vpc.smoke.default_route_table_id}"
  tags = {
      Name = "uat-default-route"
  }
}