resource "aws_internet_gateway" "smoke_igw" {
 vpc_id = "${aws_vpc.smoke.id}"
 tags = {
    Name = "smoke-igw"
 }
}